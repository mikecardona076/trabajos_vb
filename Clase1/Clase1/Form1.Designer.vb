﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.BtnclickThis = New System.Windows.Forms.Button()
        Me.HelloWorld = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'BtnclickThis
        '
        Me.BtnclickThis.Location = New System.Drawing.Point(335, 240)
        Me.BtnclickThis.Name = "BtnclickThis"
        Me.BtnclickThis.Size = New System.Drawing.Size(75, 23)
        Me.BtnclickThis.TabIndex = 0
        Me.BtnclickThis.Text = "Oprime"
        Me.BtnclickThis.UseVisualStyleBackColor = True
        '
        'HelloWorld
        '
        Me.HelloWorld.AutoSize = True
        Me.HelloWorld.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.HelloWorld.Location = New System.Drawing.Point(348, 92)
        Me.HelloWorld.Name = "HelloWorld"
        Me.HelloWorld.Size = New System.Drawing.Size(62, 13)
        Me.HelloWorld.TabIndex = 1
        Me.HelloWorld.Text = "Hello World"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.MenuHighlight
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.HelloWorld)
        Me.Controls.Add(Me.BtnclickThis)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents BtnclickThis As Button
    Friend WithEvents HelloWorld As Label
End Class
