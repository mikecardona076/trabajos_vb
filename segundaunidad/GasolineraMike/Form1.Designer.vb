﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.mike = New System.Windows.Forms.Button()
        Me.diesel = New System.Windows.Forms.Button()
        Me.classic = New System.Windows.Forms.Button()
        Me.total = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Ravie", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(29, 40)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(267, 34)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "GASOLINA MIKE"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Ravie", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(319, 116)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(251, 36)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "DIESEL MIKE"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Ravie", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(569, 38)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(280, 36)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "MIKE CLASSIC "
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(127, 88)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(36, 20)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "19$"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(422, 173)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(36, 20)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "30$"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(673, 88)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(36, 20)
        Me.Label6.TabIndex = 5
        Me.Label6.Text = "25$"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(390, 312)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(100, 20)
        Me.TextBox1.TabIndex = 6
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(270, 315)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(97, 13)
        Me.Label7.TabIndex = 7
        Me.Label7.Text = "Ingresa la cantidad"
        '
        'mike
        '
        Me.mike.Location = New System.Drawing.Point(256, 364)
        Me.mike.Name = "mike"
        Me.mike.Size = New System.Drawing.Size(111, 23)
        Me.mike.TabIndex = 8
        Me.mike.Text = "Gasolina Mike"
        Me.mike.UseVisualStyleBackColor = True
        '
        'diesel
        '
        Me.diesel.Location = New System.Drawing.Point(397, 364)
        Me.diesel.Name = "diesel"
        Me.diesel.Size = New System.Drawing.Size(75, 23)
        Me.diesel.TabIndex = 9
        Me.diesel.Text = "Diesel Mike"
        Me.diesel.UseVisualStyleBackColor = True
        '
        'classic
        '
        Me.classic.Location = New System.Drawing.Point(495, 364)
        Me.classic.Name = "classic"
        Me.classic.Size = New System.Drawing.Size(92, 23)
        Me.classic.TabIndex = 10
        Me.classic.Text = "Mike Classic"
        Me.classic.UseVisualStyleBackColor = True
        '
        'total
        '
        Me.total.AutoSize = True
        Me.total.Location = New System.Drawing.Point(419, 425)
        Me.total.Name = "total"
        Me.total.Size = New System.Drawing.Size(39, 13)
        Me.total.TabIndex = 11
        Me.total.Text = "Label8"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.GasolineraMike.My.Resources.Resources.gasolineras_futuro_1
        Me.ClientSize = New System.Drawing.Size(912, 537)
        Me.Controls.Add(Me.total)
        Me.Controls.Add(Me.classic)
        Me.Controls.Add(Me.diesel)
        Me.Controls.Add(Me.mike)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents mike As Button
    Friend WithEvents diesel As Button
    Friend WithEvents classic As Button
    Friend WithEvents total As Label
End Class
